from models import *

db.session.add(Device(Name="Water Pipes", IP="1.2.3.4", Port="1234", Nodes=[
    Node(Name="Current Pressure", Value=600, MinVal=0, MaxVal=1000, ValueUnits="PSI", ReadOnly=True),
    Node(Name="Pressure Setpoint", Value=600, ValueUnits="PSI", ReadOnly=False)
    ])

db.session.add(Device(Name="Boiler", IP="3.2.1.1", Port="1234", Nodes=[
    Node(Name="Current Temperature", Value=400, MinVal=0, MaxVal=600, ValueUnits="F", ReadOnly=True),
    Node(Name="Temperature Setpoint", Value=400, ValueUnits="F", ReadOnly=False)
]))
db.session.commit()
