from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://flask:flask@localhost/flask'
db = SQLAlchemy(app)


class Device(db.Model):
    __tablename__ = 'device'
    DeviceID = db.Column(db.Integer, primary_key=True)
    IP = db.Column(db.String(15), nullable=False)
    Port = db.Column(db.Integer, nullable=False)
    Name = db.Column(db.String(15), nullable=False)
    Nodes = db.relationship('Node', backref='device', lazy=True)
    def __repr__(self):
        return "<Device:{}>".format(self.Name)

class Node(db.Model):
    __tablename__ = 'node'
    NodeID = db.Column(db.Integer, primary_key=True, autoincrement=True)
    Device = db.Column(db.Integer, db.ForeignKey('device.DeviceID'), primary_key=True)
    ReadOnly = db.Column(db.Boolean, default=1)
    Name = db.Column(db.Text, nullable=False)
    Value = db.Column(db.Integer, default=-1)
    ValueUnits = db.Column(db.Text)
    SendValue = db.Column(db.Boolean, default=0)
    MinVal = db.Column(db.Integer, default=0)
    MaxVal = db.Column(db.Integer, default=-1) #-1 becomes "not defined" for us. Maybe we key off this for rendering?
    NodeHistory = db.relationship('NodeHistory', backref='node', lazy=True)
    def __repr__(self):
        return "<Node: {}:{}>".format(self.device.Name, self.Name)

class NodeHistory(db.Model):
    __tablename__ = 'nodehistory'
    Device = db.Column(db.ForeignKey("device.DeviceID"), primary_key=True)
    Node = db.Column(db.ForeignKey("node.NodeID"), primary_key=True)
    UpdateTime = db.Column(db.DateTime, default=datetime.utcnow, primary_key=True)
    Value = db.Column(db.Integer, nullable=False)
    def __repr__(self):
        return "<NodeHistory: {}-{}: {}>".format(self.device.Name, self.Name, self.UpdateTime)
