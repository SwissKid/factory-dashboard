from flask import Flask, render_template, jsonify, request
import time
app = Flask(__name__)
app.config.update(
    TEMPLATES_AUTO_RELOAD = True
)
app.config["TEMPLATES_AUTO_RELOAD"] = True
from models import *

@app.route('/')
def hello_world():
    return render_template("home.html")

@app.route('/dashboard')
def dashboard():
    devices = Device.query.options(db.joinedload('Nodes')).all()
    t = 0
    try:
        with open("/tmp/lastpull", "r") as f:
            t = float(f.readline())
            diff = time.time() - t
            if diff > 150:
                return "Cannot communicate with the factory"
    except:
        return "Cannot communicate with the factory"
    return render_template("test.html", devices=devices)

@app.route('/admin')
def admin():
    writeable_nodes = Node.query.filter_by(ReadOnly=0).all()
    devices = Device.query.all()
    return render_template("admin.html", nodes=writeable_nodes, devices=devices)

@app.route('/api/get/<int:deviceid>/<int:nodeid>')
def get_status(deviceid, nodeid):
    node = Node.query.filter_by(Device=deviceid, NodeID=nodeid).first_or_404()
    return jsonify({"value": node.Value, "node": node.NodeID, "device": node.device.DeviceID})

@app.route('/api/set/<int:deviceid>/<int:nodeid>', methods=['GET','POST'])
def update_node(deviceid, nodeid):
    node = Node.query.filter_by(Device=deviceid, NodeID=nodeid).first_or_404()
    if node.ReadOnly:
        return jsonify({"success": False, "reason": "Node is read only"})
    node.Value = request.values["value"]
    node.SendValue = True
    db.session.commit()
    return jsonify({"success": True})
